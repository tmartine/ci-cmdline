"""unwrap utility function
Copied from: https://www.reddit.com/r/Python/comments/r6m0lb/comment/hmum7th/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
"""

from typing import Optional, TypeVar


class UnwrapError(Exception):
    pass


T = TypeVar("T")


def unwrap(value: Optional[T]) -> T:
    """
    Performs unwrapping of optional types and raises `UnwrapError` if the value is None. Useful for
    instances where a value of some optional type is required to not be None; raising an exception
    if None is encountered.

    :param value: value of an optional type
    :return: the value if not None
    """
    if value is None:
        raise UnwrapError("Expected value of optional type to not be None")
    return value
