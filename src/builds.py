"""Managing Jenkins builds"""

import pathlib
import typing

import api.spec
import commandline
import connections
import tables
import projects
import jobs

LIST_SPEC = {
    "number": ("number", int),
    "_class": "_class",
    "building": "building",
    "result": "result",
}

ListFilter = typing.NewType("ListFilter", str)
tables.init_list_filter_type(ListFilter, LIST_SPEC)


def complete_build(**kwargs):
    """complete build"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        build_list = get_list(connection, parsed_args.job)
        return ["last"] + [str(build["number"]) for build in build_list]


BuildNumber = typing.NewType("BuildNumber", str)
BuildNumber.__doc__ = """\
    single build

    BUILD can be designated either a (positive) build number, or 'last' or 0 to
    refer to the last build, or a negative number '-i' to refer to the ith
    previous build.
    """
commandline.set_completer(BuildNumber, complete_build)

Range = typing.NewType("Range", str)
Range.__doc__ = """\
    single build or build range

    BUILD can be either a single build or a range i-j between two builds.
    Each build can be designated either by a (positive) build number,
    or 'last' or 0 to refer to the last build, or a negative number '-i' to
    refer to the ith previous build.
    """
commandline.set_completer(Range, complete_build)

BuildFollow = typing.NewType("BuildFollow", bool)
BuildFollow.__doc__ = "keep showing progress until the job is completed"
commandline.add_alias(BuildFollow, "-f")


@commandline.RegisterCategory
class Build(commandline.Category):
    """managing Jenkins builds"""

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        job: jobs.Buildable,
        list_filter: typing.Iterable[ListFilter],
    ) -> None:
        """list job builds"""
        jobs.simple_list(self.config, project, job, list_filter)

    @commandline.RegisterAction
    def log(
        self,
        project: projects.Name,
        job: jobs.Buildable,
        build: BuildNumber,
        follow: BuildFollow = BuildFollow(False),
    ) -> None:
        """show build log"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            build_number = str_to_build_number(
                build, lambda: get_list(connection, job)
            )
            connection.jenkins.show_build_log(job, build_number, follow=follow)

    @commandline.RegisterAction
    def replay(
        self,
        project: projects.Name,
        job: jobs.Buildable,
        build: BuildNumber,
        filename: commandline.Filename,
    ) -> None:
        """replay build with a given Jenkinsfile"""
        jenkinsfile = pathlib.Path(filename).read_text()
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            build_number = str_to_build_number(
                build, lambda: get_list(connection, job)
            )
            connection.jenkins.replay_build(job, build_number, jenkinsfile)

    @commandline.RegisterAction
    def validate(
        self, project: projects.Name, filename: commandline.Filename
    ) -> None:
        """validate Jenkinsfile"""
        jenkinsfile = pathlib.Path(filename).read_text()
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            print(connection.jenkins.pipeline_model_validate(jenkinsfile))

    @commandline.RegisterAction
    def delete(self, project: projects.Name, job: jobs.Buildable, build: Range):
        """delete builds"""
        self.__command(
            (lambda jenkins: jenkins.delete_build), project, job, build
        )

    @commandline.RegisterAction
    def stop(self, project: projects.Name, job: jobs.Buildable, build: Range):
        """stop builds"""
        self.__command(
            (lambda jenkins: jenkins.stop_build), project, job, build
        )

    @commandline.RegisterAction
    def terminate(
        self, project: projects.Name, job: jobs.Buildable, build: Range
    ):
        """terminate builds"""
        self.__command(
            (lambda jenkins: jenkins.terminate_build), project, job, build
        )

    @commandline.RegisterAction
    def kill(self, project: projects.Name, job: jobs.Buildable, build: Range):
        """kill builds"""
        self.__command(
            (lambda jenkins: jenkins.kill_build), project, job, build
        )

    def __command(self, method, project, job, build):
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            for build_number in str_to_build_set(build, connection, job):
                method(connection.jenkins)(job, build_number)


def str_to_build_number(build: str, lazy_build_list) -> int:
    """convert a relative build number to an effective build number"""
    if build == "last":
        build_number = 0
    else:
        build_number = commandline.str_to_int(build)
    if build_number <= 0:
        build_number = lazy_build_list()[-build_number]["number"]
    return build_number


def str_to_build_set(build: str, connection, job) -> typing.Iterable[int]:
    """convert a build range to a list of effective build numbers"""
    try:
        build_from, build_to = build.split("-")
        build_list = get_list(connection, job)
        build_number_from = str_to_build_number(build_from, lambda: build_list)
        build_number_to = str_to_build_number(build_to, lambda: build_list)
        build_numbers = {build["number"] for build in build_list}
        return [
            build
            for build in range(build_number_from, build_number_to + 1)
            if build in build_numbers
        ]
    except ValueError:
        return [str_to_build_number(build, lambda: get_list(connection, job))]


def get_list(connection, job):
    """enumerate all job builds"""
    build_list = connection.jenkins.get_job_info(job, depth=1)["builds"]
    return api.spec.remap_dict_list(build_list, LIST_SPEC)
