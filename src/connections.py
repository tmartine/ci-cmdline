"""Managing credentials and network connections with ci.inria.fr and
Cloudstack"""

import getpass
import os
import typing
import yaml

import readchar
import requests

import api.cloudstack
import api.connection
import api.jenkins_ext
import api.portal
import commandline

CI_URL = "ci.inria.fr"
QLF_CI_URL = "qlf-ci.inria.fr"
CLOUDSTACK_URL = "https://sesi-cloud-ctl1.inria.fr/client"
QLF_CLOUDSTACK_URL = "https://qlf-sesi-cloud-ctl1.inria.fr/client"


def create_private_file(filename: str) -> typing.TextIO:
    """open a new file for writing, readable and writeable only for the owner"""
    return open(os.open(filename, os.O_CREAT | os.O_WRONLY, 0o600), "w")


class Credentials:
    """credentials management"""

    Store = typing.Dict[str, typing.Any]

    def __init__(
        self,
        credentials_filename: typing.Optional[str],
        site: str,
        interactive: bool,
    ):
        self.__credentials_filename = credentials_filename
        self.__credentials_asked = False
        if credentials_filename is None:
            self.__save_credentials: typing.Optional[bool] = False
        else:
            self.__save_credentials = None
        self.__interactive = interactive
        self.__stores: typing.Optional[Credentials.Store] = None
        self.__site = site
        try:
            if credentials_filename is not None:
                with open(credentials_filename) as file:
                    try:
                        self.__stores = yaml.load(file, Loader=yaml.FullLoader)
                    except yaml.scanner.ScannerError as exc:
                        try:
                            with open(
                                credentials_filename + ".python_debug"
                            ) as pyfile:
                                print(pyfile.read())
                        except FileNotFoundError:
                            pass
                        raise exc
                    self.__save_credentials = True
                if "username" in self.__stores:
                    self.__stores = {"ci.inria.fr": self.__stores}
        except FileNotFoundError:
            pass

    # Quoting Store to satisfy pylint
    def read_from_stdin(self) -> typing.Optional["Store"]:
        """read credentials from stdin"""
        if self.__credentials_asked or not self.__interactive:
            return None
        print(f"User name for {self.__site}: ", end="")
        username = input()
        password = getpass.getpass()
        store = {"username": username, "password": password}
        stores = self.__stores
        if stores is None:
            stores = {}
        stores[self.__site] = store
        self.update(store)
        self.__stores = stores
        self.__credentials_asked = True
        self.__save_credentials = None
        return store

    def obtain(self) -> api.connection.Credentials:
        """return the current credentials"""
        store = self.maybe_store
        if not store:
            store = self.read_from_stdin()
            if not store:
                raise commandline.Failure("No credentials available")
        return api.connection.Credentials(
            username=store["username"], password=store["password"]
        )

    @property
    def store(self) -> "Store":
        """get stored credentials"""
        result = self.maybe_store
        if result is None:
            raise commandline.Failure("No credentials available")
        return result

    @property
    def maybe_store(self) -> typing.Optional["Store"]:
        """get stored credentials or None if not available"""
        stores = self.__stores
        if stores is None:
            return None
        return stores.get(self.__site, None)

    def forget(self, key: str) -> None:
        """forget a stored credentials key"""
        store = self.maybe_store
        if store is not None:
            del store[key]

    # Quoting Store to satisfy pylint
    def update(self, store: "Store") -> None:
        """update persistent credentials"""
        stores = self.__stores
        if stores is None:
            stores = {}
            self.__stores = stores
        stores[self.__site] = store
        credentials_filename = self.__credentials_filename
        save_credentials = self.__save_credentials
        if save_credentials is None and self.__interactive:
            print(f"Save to {credentials_filename}? (y/N) ", end="", flush=True)
            save_credentials = readchar.readchar() == "y"
            self.__save_credentials = save_credentials
        # credentials_filename != None is implied by save_credentials
        # but the typer does not know it.
        if save_credentials and credentials_filename is not None:
            # with create_private_file(
            #     credentials_filename + ".python_debug"
            # ) as file:
            #     print(stores, file=file)
            with create_private_file(credentials_filename) as file:
                yaml.dump(stores, file)


class CredentialsProvider(api.connection.CredentialsProvider):
    """credentials provider interface"""

    def __init__(
        self,
        credentials: Credentials,
        session: requests.Session,
        cookies_key: str,
    ):
        self.__credentials = credentials
        self.__cookies_key = cookies_key
        self.__fetch_cookies_from_credentials(session.cookies)

    def __fetch_cookies_from_credentials(self, cookies):
        store = self.__credentials.maybe_store
        if not store:
            return
        try:
            cookies_dict = store[self.__cookies_key]
        except KeyError:
            return
        requests.utils.add_dict_to_cookiejar(cookies, cookies_dict)

    def get_credentials(self) -> api.connection.Credentials:
        return self.__credentials.obtain()

    def store_cookies(self, cookies):
        cookies_dict = requests.utils.dict_from_cookiejar(cookies)
        store = self.__credentials.store
        store[self.__cookies_key] = cookies_dict
        self.__credentials.update(store)


ConnectMethod = typing.Callable[[], requests.Response]

# T seems legit for a type variable!
# pylint: disable=invalid-name
T = typing.TypeVar("T")


class Connections:
    """Portal, CloudStack and Jenkins connection management with credentials"""

    def __init__(
        self,
        config: commandline.Config,
        subdomain=None,
        interactive=True,
        ci_admin=False,
    ):
        self.__config = config
        site = self.ci_url
        credentials = Credentials(config.credentials, site, interactive)
        self.__credentials = credentials
        portal_connection = requests.Session()
        if config.proxy is not None:
            portal_connection.proxies = {"https": config.proxy}
        self.__portal = api.portal.Portal(
            "https://" + site,
            portal_connection,
            CredentialsProvider(credentials, portal_connection, "ci-cookies"),
            config.timeout,
        )
        self.__cloudstack = api.lazy.lazy(self.__init_cloudstack)
        self.__subdomain = subdomain
        self.__jenkins = api.lazy.lazy(self.__init_jenkins)
        self.__ci_admin = ci_admin

    @property
    def ci_url(self):
        """return CI URL"""
        if self.__config.qualif:
            return QLF_CI_URL
        return CI_URL

    @property
    def cloudstack_url(self):
        """return CloudStack URL"""
        if self.__config.qualif:
            return QLF_CLOUDSTACK_URL
        return CLOUDSTACK_URL

    @property
    def cloudstack_api_url(self):
        """return CloudStack API URL"""
        if self.__config.qualif:
            return QLF_CLOUDSTACK_URL + "/api"
        return CLOUDSTACK_URL + "/api"

    @property
    def cloudstack_console_url(self):
        """return CloudStack console URL"""
        if self.__config.qualif:
            return QLF_CLOUDSTACK_URL + "/console"
        return CLOUDSTACK_URL + "/console"

    def __enter__(self):
        return self

    def __exit__(self, _type, _value, _traceback):
        portal = self.__portal
        if portal:
            portal.session.close()
        cloudstack = self.__cloudstack
        if cloudstack.computed:
            self.__cloudstack().session.close()
        jenkins = self.__jenkins
        if jenkins.computed:
            self.__jenkins().close()

    @property
    def portal(self) -> api.portal.Portal:
        """return portal connection"""
        return self.__portal

    @property
    def cloudstack(self) -> api.cloudstack.CloudStackExt:
        """return CloudStack connection"""
        return self.__cloudstack()

    def __init_cloudstack(self) -> api.cloudstack.CloudStackExt:
        if self.__ci_admin:
            subdomain = None
        else:
            subdomain = self.__subdomain
        return api.cloudstack.CloudStackExt(
            self.cloudstack_api_url,
            self.credentials.obtain(),
            subdomain,
            timeout=self.__config.timeout,
            fetch_result=True,
        )

    def get_project_domainid(self, project):
        """find domain id for project when connected to Cloudstack
        as CI admin"""
        response = self.cloudstack.listDomains(
            listAll=True, domainid=self.cloudstack.domainid, name=project
        )
        return response["domain"][0]["id"]

    def cloudstack_args(self):
        """return Cloudstack arguments according to ci_admin flag"""
        if self.__ci_admin and self.__subdomain != "":
            return {"domainid": self.get_project_domainid(self.__subdomain)}
        return {}

    @property
    def jenkins(self) -> api.jenkins_ext.JenkinsExt:
        """return Jenkins connection"""
        return self.__jenkins()

    def __init_jenkins(self) -> api.jenkins_ext.JenkinsExt:
        if self.__subdomain is None:
            raise commandline.Failure("No Jenkins connection without project")
        credentials = self.credentials.obtain()
        return api.jenkins_ext.JenkinsExt(
            f"https://{self.ci_url}/{self.__subdomain}",
            credentials.username,
            credentials.password,
        )

    @property
    def credentials(self) -> Credentials:
        """return credentials"""
        return self.__credentials

    @property
    def username(self) -> str:
        """return user name registered in credentials"""
        return self.credentials.obtain().username

    @property
    def zone_id(self) -> str:
        """return the project zone id"""
        response = self.cloudstack.listZones()
        return response["zone"][0]["id"]

    @property
    def subdomain(self) -> str:
        """return subdomain"""
        return self.__subdomain

    def retry_loop(self, command: typing.Callable[[], T]) -> T:
        """retry the command according to the --retry argument"""
        retry_count = self.__config.retry
        while True:
            try:
                result = command()
            except requests.exceptions.RequestException as exc:
                if retry_count > 0:
                    retry_count -= 1
                    continue
                raise exc
            return result


class NoninteractiveConnections(Connections):
    """Non-interactive connections for command-line completers"""

    def __init__(self, credentials_filename: str, subdomain=None):
        config: commandline.Config = commandline.Config(
            verbose=False,
            quiet=True,
            credentials=credentials_filename,
            timeout=commandline.DEFAULT_TIMEOUT,
            retry=commandline.DEFAULT_RETRY,
            qualif=False,
            proxy=None,
            compact=False,
        )
        super().__init__(config, interactive=False, subdomain=subdomain)
