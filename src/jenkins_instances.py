"""Managing Jenkins instances"""

import typing

import commandline
import connections
import projects

Qualif = typing.NewType("Qualif", bool)
Qualif.__doc__ = "Jenkins qualification instance [default: production instance]"


def complete_version(**kwargs):
    """completer for version arguments"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, parsed_args.project
    ) as connection:
        versions = connection.portal.projects[
            parsed_args.project
        ].jenkins.get_available_versions()
        return ["latest", "latest-lts"] + list(versions.keys())


Version = typing.NewType("Version", str)
Version.__doc__ = "version number or 'latest' or 'latest-lts'"
commandline.set_completer(Version, complete_version)

Check = typing.NewType("Check", bool)
Check.__doc__ = "check availability only (do not install)"

NoRestart = typing.NewType("NoRestart", bool)
NoRestart.__doc__ = "do not restart Jenkins"


@commandline.RegisterCategory
class Jenkins(commandline.Category):
    """managing Jenkins instances"""

    @commandline.RegisterAction
    def start(
        self, project: projects.Name, qualif: Qualif = Qualif(False)
    ) -> None:
        """start Jenkins instance"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].jenkins.start(qualif)

    @commandline.RegisterAction
    def restart(
        self, project: projects.Name, qualif: Qualif = Qualif(False)
    ) -> None:
        """restart Jenkins instance"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].jenkins.restart(qualif)

    @commandline.RegisterAction
    def stop(
        self, project: projects.Name, qualif: Qualif = Qualif(False)
    ) -> None:
        """stop Jenkins instance"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].jenkins.stop(qualif)

    @commandline.RegisterAction
    def install(self, project: projects.Name, version: Version) -> None:
        """install new Jenkins version on the qualification area"""
        with connections.Connections(self.config) as connection:
            versions = connection.portal.projects[
                project
            ].jenkins.get_available_versions()
            if version == "latest":
                version_obj = next(iter(versions.values()))
            elif version == "latest-lts":
                version_obj = next(
                    version for version in versions.values() if version.lts
                )
            else:
                try:
                    version_obj = versions[version]
                except KeyError:
                    raise commandline.Failure(
                        f"Unknown Jenkins version: {version}"
                    )
            version_obj.install_qualif()

    @commandline.RegisterAction
    def sync(self, project: projects.Name) -> None:
        """replicate continuous integration configuration / jobs
        from production to qualification"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].jenkins.push()

    @commandline.RegisterAction
    def push(self, project: projects.Name) -> None:
        """push qualification version on production"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].jenkins.install_prod()

    @commandline.RegisterAction
    def rollback(self, project: projects.Name) -> None:
        """go back to the previous revision installed on production"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].jenkins.rollback()

    @commandline.RegisterAction
    def upgrade(
        self,
        project: projects.Name,
        check: Check = Check(False),
        no_restart=NoRestart(False),
    ) -> None:
        """upgrade Jenkins to the last available version"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            available = connection.jenkins.check_available_upgrade()
            if available is None:
                raise commandline.Failure(f"No upgrade available")
            print(available)
            if check:
                return
            connection.jenkins.upgrade()
            if not no_restart:
                connection.jenkins.safe_restart()

    @commandline.RegisterAction
    def safe_restart(self, project: projects.Name) -> None:
        """restart Jenkins after job completion"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            connection.jenkins.safe_restart()


def get_instance_name(project: projects.Name, qualif: Qualif) -> str:
    """return the instance name: either project or project-qualif"""
    if qualif:
        return project + "-qualif"
    return project
