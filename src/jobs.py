"""Managing Jenkins jobs"""

import sys
import typing

import jenkins

import api.spec
import commandline
import connections
import agents
import projects
import volumes
import tables
import tokens

# See https://github.com/python/typeshed/issues/3500#issuecomment-560958608
if sys.version_info >= (3, 8):
    typing_extensions = typing  # pylint: disable=invalid-name
else:
    import typing_extensions

ShortName = typing.NewType("ShortName", str)
ShortName.__doc__ = "short name"

DisplayName = typing.NewType("DisplayName", str)
DisplayName.__doc__ = "display name"

Description = typing.NewType("Description", str)
Description.__doc__ = "description"

ScriptPath = typing.NewType("ScriptPath", str)
ScriptPath.__doc__ = "Jenkinsfile path"

Remote = typing.NewType("Remote", str)


class Git(typing_extensions.TypedDict):
    "Git source"
    source_type: typing_extensions.Literal["git"]
    remote: Remote


class GitHub(typing_extensions.TypedDict):
    "GitHub source"
    source_type: typing_extensions.Literal["github"]
    remote: Remote


Source = typing.Union[Git, GitHub]

LIST_SPEC = {"name": "name", "_class": "_class"}

JobListFilter = typing.NewType("JobListFilter", str)
tables.init_list_filter_type(JobListFilter, LIST_SPEC)


def complete_name(**kwargs):
    """complete job name"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        jobs = get_list(connection)
        return [job["name"] for job in jobs]


Name = typing.NewType("Name", str)
Name.__doc__ = """job name"""
commandline.set_completer(Name, complete_name)


def complete_buildable_job(**kwargs):
    """complete job or sub-job name"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        return get_buildable_job_list(connection)


Buildable = typing.NewType("Buildable", str)
Buildable.__doc__ = """job to build"""
commandline.set_completer(Buildable, complete_buildable_job)


@commandline.RegisterCategory
class Job(commandline.Category):
    """managing Jenkins jobs"""

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        list_filter: typing.Iterable[JobListFilter],
        job: typing.Optional[Name] = None,
    ) -> None:
        """list jobs

        If JOB is given, sub-jobs of JOB are listed.
        """
        simple_list(self.config, project, job, list_filter)

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def create(
        self,
        project: projects.Name,
        short_name: ShortName,
        display_name: DisplayName,
        sources: typing.Iterable[Source],
        description: Description = Description(""),
        script_path: ScriptPath = ScriptPath("Jenkinsfile"),
    ) -> None:
        """create a new job"""
        loader = agents.new_template_loader()
        template = loader.load("pipeline-job.xml")
        config = template.generate(
            display_name=display_name,
            sources=sources,
            description=description,
            script_path=script_path,
        ).render()
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            connection.jenkins.create_job(short_name, config)

    @commandline.RegisterAction
    def build(self, project: projects.Name, job: Buildable) -> None:
        """build job"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            connection.jenkins.build_job(job)

    @commandline.RegisterAction
    def delete(
        self, project: projects.Name, job: typing.Iterable[Name]
    ) -> None:
        """delete jobs

        JOB are regular expressions which match job names.
        Multiple matches are accepted, but the command will
        fail if a regular expression has no match."""
        invalid = []
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            job_list = get_list(connection)
            jobs = [
                a_job
                for job_pattern in job
                for a_job in find(job_list, job_pattern)
            ]
            for a_job in jobs:
                try:
                    connection.jenkins.delete_job(a_job["name"])
                except jenkins.JenkinsException as error:
                    invalid.append(f"{a_job} ({error})")
        commandline.report_invalid(invalid)

    @commandline.RegisterAction
    def new_build_hook(
        self,
        project: projects.Name,
        job: Name,
        token: typing.Optional[tokens.NewName] = None,
    ) -> None:
        """print an URL with a fresh token for running a build"""
        if token is None:
            token = tokens.NewName(f"build hook for {job}")
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            uuid = connection.jenkins.create_token(
                username=connection.username, name=token
            )
            print(
                connection.jenkins.get_build_hook_url(
                    job, connection.username, uuid
                )
            )


def simple_list(config, project, job, list_filter):
    """implements simple list for jobs"""
    volumes.simple_list(
        config,
        project,
        list_filter,
        lambda connection: get_list(connection, job),
    )


def get_job_path(job: str) -> typing.Iterable[str]:
    """return job path for Jenkins URL"""
    try:
        job, sub_job = job.split("/")
        return [job, "job", sub_job]
    except ValueError:
        return [job]


def get_list(
    connection: connections.Connections, parent_job: typing.Optional[str] = None
):
    """enumerate all jobs"""
    if parent_job is None:
        jobs = connection.jenkins.get_all_jobs(folder_depth=0)
    else:
        jobs = connection.jenkins.get_job_info(parent_job)["jobs"]
    return api.spec.remap_dict_list(jobs, LIST_SPEC)


BUILDABLE_JOBS = [
    "hudson.model.FreeStyleProject",
    "org.jenkinsci.plugins.workflow.job.WorkflowJob",
]

ITERABLE_JOBS = [
    "org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject"
]


def get_buildable_job_list(connection):
    """enumerate all buildable jobs"""
    jobs = get_list(connection)
    buildable_jobs = []
    for job in jobs:
        name = job["name"]
        class_ = job["_class"]
        if class_ in BUILDABLE_JOBS:
            buildable_jobs.append(name)
        if class_ in ITERABLE_JOBS:
            for sub_job in get_list(connection, parent_job=name):
                if sub_job["_class"] in BUILDABLE_JOBS:
                    buildable_jobs.append(name + "/" + sub_job["name"])
    return buildable_jobs


def find(job_list, job_pattern):
    """find job from pattern"""
    job_dict = {job["name"]: job for job in job_list}
    matches = list(tables.match_dict(job_dict, job_pattern))
    if not matches:
        raise commandline.Failure(f"{job_pattern} does not match any job")
    return [job for _job_name, job in matches]
