"""Managing templates"""

import os
import textwrap
import typing

import wget
import yaml

import api.spec
import commandline
import connections
import projects
import tables
import vms
import volumes


def complete_template(**kwargs):
    """completer for template arguments"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, parsed_args.project
    ) as connection:
        template_list = get_list(connection)
        return [
            name
            for template in template_list
            for name in [template["id"], template["name"]]
        ]


Name = typing.NewType("Name", str)
Name.__doc__ = "template name"
commandline.set_completer(Name, complete_template)


KVM_FORMATS = ["QCOW2", "RAW", "VHD", "VMDK"]

VMWARE_FORMATS = ["OVA"]


def complete_format(**_kwargs):
    """completer for format"""
    return KVM_FORMATS + VMWARE_FORMATS


Format = typing.NewType("Format", str)
Format.__doc__ = "template format"
commandline.set_completer(Format, complete_format)


def guess_format(format_name: typing.Optional[str], filename: str) -> str:
    """guess format"""
    if format_name is None:
        _, ext = os.path.splitext(filename)
        if ext == "":
            raise commandline.Failure("Cannot guess --format")
        format_str = ext[1:]
    else:
        format_str = format_name
    return format_str.upper()


def hypervisor_of_format(format_name):
    """return the hypervisor that supports the given format"""
    if format_name in KVM_FORMATS:
        return "KVM"
    if format_name in VMWARE_FORMATS:
        return "VMWare"
    raise commandline.Failure("Unknown format: " + format_name)


def complete_os_type(**kwargs):
    """complete arguments for OS type"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        return vms.dict_of_os_types(connection).values()


def complete_create(**kwargs):
    """complete arguments for create command"""
    parsed_args = kwargs["parsed_args"]
    step = commandline.get_complete_argument_index(parsed_args)
    if step == 0:  # volume
        return volumes.complete_volume(parsed_args=parsed_args)
    return None


LIST_SPEC: api.spec.DictSpec = {
    "id": "id",
    "name": "name",
    "description": "displaytext",
    "os_type": "ostypename",
    "public": "ispublic",
    "featured": "isfeatured",
    "dynamically_scalable": "isdynamicallyscalable",
    "extractable": "isextractable",
    "_zoneid": "zoneid",
}

TemplateListFilter = typing.NewType("TemplateListFilter", str)
tables.init_list_filter_type(TemplateListFilter, LIST_SPEC)

TemplateCreateParameter = typing.NewType("TemplateCreateParameter", str)
TemplateCreateParameter.__doc__ = (
    "[YAML-FILE | VOLUME TEMPLATE-NAME DESCRIPTION]"
)
commandline.set_completer(TemplateCreateParameter, complete_create)

OsType = typing.NewType("OsType", str)
OsType.__doc__ = """OS type

    OS-TYPE can be an id or a regular expression that matches the
    corresponding OS
    type without ambiguity (the command will fail if there is no match or
    two matches or more)."""
commandline.set_completer(OsType, complete_os_type)

Public = typing.NewType("Public", bool)
Public.__doc__ = "allow template to be used by other projects"

PasswordEnabled = typing.NewType("PasswordEnabled", bool)
PasswordEnabled.__doc__ = "enable reset password feature"

Featured = typing.NewType("Featured", bool)
Featured.__doc__ = "include template in featured template list"

DynamicallyScalable = typing.NewType("DynamicallyScalable", bool)
DynamicallyScalable.__doc__ = """
    specify that the template contains XS/VMWare tools to support dynamic
    scaling
    """

Extractable = typing.NewType("Extractable", bool)
Extractable.__doc__ = """
    enable the template to be extracted
    """

PrintUrl = typing.NewType("PrintUrl", bool)
PrintUrl.__doc__ = "print the URL instead of downloading"

IsUrl = typing.NewType("IsUrl", bool)
IsUrl.__doc__ = "specify that an URL is given (default: filename)"

FilenameOrURL = typing.NewType("FilenameOrURL", commandline.Filename)
FilenameOrURL.__doc__ = "filename or URL of the template to register"

TEMPLATE_NAME_SPEC: api.spec.TextSpec = {
    """must be between 1 and 31 characters long""": lambda s: 1 <= len(s) <= 31
}

FILTER_NAME_LIST = ["all", "featured", "community"]


def complete_project_or_filter_name(**kwargs):
    """complete project or filter arguments"""
    return FILTER_NAME_LIST + projects.complete_project(**kwargs)


ProjectOrFilterName = typing.NewType("ProjectOrFilterName", str)
ProjectOrFilterName.__doc__ = """
    project name or "all", "featured" or "community"
"""
commandline.set_completer(ProjectOrFilterName, complete_project_or_filter_name)


class TemplateFilter(typing.NamedTuple):
    """template filter from project name"""

    subdomain: str
    templatefilter: str


def _get_template_filter(project: ProjectOrFilterName) -> TemplateFilter:
    if project in FILTER_NAME_LIST:
        subdomain = ""
        templatefilter: str = project
    else:
        subdomain = project
        templatefilter = "self"
    return TemplateFilter(subdomain=subdomain, templatefilter=templatefilter)


@commandline.RegisterCategory
class Template(commandline.Category):
    """managing templates"""

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction(
        doc=f"""creates a new template.

        If no additional argument is provided, the command outputs a YAML
        template.
        This output can be redirected into a file to be filled and provided back
        to the command to perform the creation.

        The template specification can also be given directly in the command
        arguments.
        {api.spec.get_spec_text('TEMPLATE-NAME', TEMPLATE_NAME_SPEC)}
        VOLUME is the volume, which the template will be created from.

        OS-TYPE can be omitted if the volume VOLUME is attached to a
        virtual machine: the OS type of the virtual machine will be used (but
        the command will fail if the volume is not attached to any virtual
        machine)."""
    )
    def create(
        self,
        project: projects.Name,
        parameter: typing.List[TemplateCreateParameter],
        os_type: typing.Optional[OsType] = None,
        public: Public = Public(False),
        password_enabled: PasswordEnabled = PasswordEnabled(False),
        featured: Featured = Featured(False),
        dynamically_scalable: DynamicallyScalable = DynamicallyScalable(False),
    ) -> None:
        "creates a new template."
        if not parameter:
            with connections.Connections(
                self.config, subdomain=project
            ) as connection:
                prepare_template_create(connection)
        else:
            new_template = Create()
            if len(parameter) == 1:
                new_template.load_yaml(parameter[0])
            else:
                try:
                    volume, name, description = parameter
                except ValueError:
                    raise commandline.Failure(
                        """
                        'template create' expect 3 arguments:
                        VOLUME, NAME, DESCRIPTION"""
                    )
                new_template.volume = volumes.Name(volume)
                new_template.options["name"] = name
                new_template.options["displayText"] = description
                new_template.os_type = os_type
                new_template.options["isPublic"] = public
                new_template.options["passwordEnabled"] = password_enabled
                new_template.options["isFeatured"] = featured
                new_template.options[
                    "isDynamicallyScalable"
                ] = dynamically_scalable
            with connections.Connections(
                self.config, subdomain=project
            ) as connection:
                new_template.create(connection, project)

    @commandline.RegisterAction
    def list(
        self,
        project: ProjectOrFilterName,
        list_filter: typing.Iterable[TemplateListFilter],
    ) -> None:
        """list project templates"""
        templatefilter = _get_template_filter(project)
        with connections.Connections(
            self.config, subdomain=templatefilter.subdomain
        ) as connection:
            args = connection.cloudstack_args()
            templates = connection.cloudstack.listTemplates(
                fetch_list=True,
                templatefilter=templatefilter.templatefilter,
                **args,
            )
            rows = api.spec.remap_dict_list(templates, LIST_SPEC)
        tables.filter_and_show_list(rows, list_filter, self.config.compact)

    @commandline.RegisterAction
    def update(
        self,
        project: projects.Name,
        template: Name,
        name: typing.Optional[str] = None,
        description: typing.Optional[str] = None,
        os_type: typing.Optional[OsType] = None,
        password_enabled: typing.Optional[PasswordEnabled] = None,
        dynamically_scalable: typing.Optional[DynamicallyScalable] = None,
        public: typing.Optional[Public] = None,
        featured: typing.Optional[Featured] = None,
    ):
        """update template properties"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            template_dict = tables.dict_of_list(get_list(connection), "id")
            template_id = find(template_dict, template)
            options = {}
            if name is not None:
                options["name"] = name
            if description is not None:
                options["displaytext"] = description
            if os_type is not None:
                os_types = vms.dict_of_os_types(connection)
                options["ostypeid"] = tables.find_offer(
                    os_types, os_type, "OS type"
                )
            if password_enabled is not None:
                options["passwordenabled"] = str(password_enabled)
            if dynamically_scalable is not None:
                options["isdynamicallyscalable"] = str(dynamically_scalable)
            if options:
                connection.cloudstack.updateTemplate(id=template_id, **options)
            options = {}
            if public is not None:
                options["ispublic"] = str(public)
            if featured is not None:
                options["isfeatured"] = str(featured)
            if options:
                connection.cloudstack.updateTemplatePermissions(
                    id=template_id, **options
                )

    @commandline.RegisterAction
    def delete(self, project: projects.Name, template: Name) -> None:
        """delete template"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            template_dict = tables.dict_of_list(get_list(connection), "id")
            template_id = find(template_dict, template)
            connection.cloudstack.deleteTemplate(
                id=template_id, zoneid=connection.zone_id, forced=False
            )
            if template_dict[template_id] == "VMware":
                commandline.output_message(
                    """\
                  Template deleted. The hypervisor is VMware: please send an
                  email to ci.infra@inria.fr to finish cleaning."""
                )

    @commandline.RegisterAction
    def download(
        self,
        project: ProjectOrFilterName,
        template: Name,
        output: typing.Optional[commandline.OutputFilename] = None,
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
        print_url: PrintUrl = PrintUrl(False),
    ) -> None:
        """\
        download a template

        If no FILENAME is given, the template name will be used instead.
        """
        templatefilter = _get_template_filter(project)
        with connections.Connections(
            self.config, subdomain=templatefilter.subdomain, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            templates = connection.cloudstack.listTemplates(
                fetch_list=True,
                templatefilter=templatefilter.templatefilter,
                **args,
            )
            template_dict = tables.dict_of_list(
                api.spec.remap_dict_list(templates, LIST_SPEC), "id"
            )
            template_id = find(template_dict, template)
            selected_template = template_dict[template_id]
            response = connection.cloudstack.extractTemplate(
                id=template_id,
                zoneid=selected_template["_zoneid"],
                mode="HTTP_DOWNLOAD",
                **args,
            )
            url = response["template"]["url"]
            if output is None:
                output = selected_template["name"] + os.path.splitext(url)[1]
            if print_url:
                print(url)
            else:
                wget.download(url, output)

    # pylint: disable=redefined-builtin
    # pylint: disable=too-many-locals
    @commandline.RegisterAction
    def register(
        self,
        project: projects.Name,
        template: Name,
        filename_or_url: FilenameOrURL,
        os_type: OsType,
        url: IsUrl = IsUrl(False),
        display_text: typing.Optional[commandline.DisplayText] = None,
        format: typing.Optional[Format] = None,
        password_enabled: typing.Optional[PasswordEnabled] = None,
        dynamically_scalable: typing.Optional[DynamicallyScalable] = None,
        public: typing.Optional[Public] = None,
        featured: typing.Optional[Featured] = None,
        extractable: Extractable = Extractable(False),
        ci_admin: vms.CiAdmin = vms.CiAdmin(False),
    ) -> None:
        """register a template"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            if display_text is None:
                displaytext: str = template
            else:
                displaytext = display_text
            format_str = guess_format(format, filename_or_url)
            hypervisor = hypervisor_of_format(format_str)
            os_types = vms.dict_of_os_types(connection)
            os_type_id = tables.find_offer(os_types, os_type, "OS type")
            args = {
                "displaytext": displaytext,
                "name": template,
                "format": format_str,
                "hypervisor": hypervisor,
                "ostypeid": os_type_id,
                "zoneid": connection.zone_id,
                "passwordenabled": password_enabled,
                "isdynamicallyscalable": dynamically_scalable,
                "ispublic": public,
                "isfeatured": featured,
                "isextractable": extractable,
            }
            if url:
                response = connection.cloudstack.registerTemplate(
                    url=filename_or_url, **args
                )
                print(response["template"][0]["id"])
            else:
                response = connection.cloudstack.getUploadParamsForTemplate(
                    **args
                )
                print(response)


class TemplateOptions(typing.TypedDict):
    name: typing.Optional[str]
    displayText: typing.Optional[str]
    description: typing.Optional[str]
    isFeatured: typing.Optional[bool]
    isPublic: typing.Optional[bool]
    passwordEnabled: typing.Optional[bool]
    isDynamicallyScalable: typing.Optional[bool]


class Create:
    """create a new template"""

    def __init__(self) -> None:
        self.options: TemplateOptions = {
            "name": None,
            "displayText": None,
            "description": None,
            "isFeatured": False,
            "isPublic": False,
            "passwordEnabled": False,
            "isDynamicallyScalable": False,
        }
        self.volume: typing.Optional[volumes.Name] = None
        self.os_type: typing.Optional[OsType] = None

    def load_yaml(self, filename: str) -> None:
        """load YAML description"""
        with open(filename) as channel:
            data = yaml.load(channel, Loader=yaml.FullLoader)
        self.options["name"] = commandline.expect_key(data, "name")
        self.options["description"] = commandline.expect_key(
            data, "description"
        )
        self.volume = commandline.expect_key(data, "volume")
        try:
            self.os_type = data["os-type"]
        except KeyError:
            pass
        try:
            self.options["isPublic"] = commandline.str_to_bool(data["public"])
        except KeyError:
            pass
        try:
            self.options["passwordEnabled"] = commandline.str_to_bool(
                data["password-enabled"]
            )
        except KeyError:
            pass
        try:
            self.options["isFeatured"] = commandline.str_to_bool(
                data["featured"]
            )
        except KeyError:
            pass
        try:
            self.options["isDynamicallyScalable"] = commandline.str_to_bool(
                data["dynamically-scalable"]
            )
        except KeyError:
            pass

    def create(
        self, connection: connections.Connections, project: projects.Name
    ) -> None:
        """create the template"""
        name = self.options["name"]
        if name is None:
            raise commandline.Failure("Template name is required.")
        api.spec.check_text_spec(name, "template name", TEMPLATE_NAME_SPEC)
        if self.options["displayText"] is None:
            raise commandline.Failure("Template description is required.")
        volume = self.volume
        if volume is None:
            raise commandline.Failure("Volume is required.")
        volume_id, volume_desc = volumes.find(connection, project, volume)
        os_type = guess_template_os_type(
            connection, self.os_type, volume, volume_desc
        )
        connection.cloudstack.createTemplate(
            volumeId=volume_id, osTypeId=os_type, **self.options
        )


def guess_template_os_type(
    connection: connections.Connections,
    os_type: typing.Optional[str],
    volume_name: str,
    volume_desc: typing.Mapping[str, str],
) -> str:
    """find template OS type"""
    if os_type is None:
        machine = volume_desc["_virtualmachineid"]
        if machine is None:
            raise commandline.Failure(
                f"""\
                '{volume_name}' is not attached to any VM:
                OS type should be given."""
            )
        vm_dict = tables.dict_of_list(
            vms.get_list_from_cloudstack(connection), "id"
        )
        return vm_dict[machine]["_guestosid"]
    os_types = vms.dict_of_os_types(connection)
    return tables.find_offer(os_types, os_type, "OS type")


def get_list(connection, **args):
    """enumerate all templates"""
    templates = connection.cloudstack.listTemplates(
        fetch_list=True, templatefilter="self", **args
    )
    return api.spec.remap_dict_list(templates, LIST_SPEC)


def find(templates, name):
    """find a template"""
    return tables.find_offer(
        {id: template["name"] for id, template in templates.items()},
        name,
        "template",
    )


def prepare_template_create(connection):
    """output YAML template for template creation"""
    os_types = vms.dict_of_os_types(connection)
    volume_dict = tables.dict_of_list(volumes.get_list(connection), "id")
    vm_dict = tables.dict_of_list(
        vms.get_list_from_cloudstack(connection), "id"
    )
    vms.print_comment(
        api.spec.get_spec_text("Template name", TEMPLATE_NAME_SPEC)
    )
    print(
        textwrap.dedent(
            """\
        name:
        description:
        # public: yes # no by default
        # password-enabled: yes # no by default
        # featured: yes # no by default
        # dynamically-scalable: yes # no by default

        ### Volume
        ## Uncomment one and only one volume id
        """
        )
    )
    for volume_id, volume in volume_dict.items():
        machine = volume["_virtualmachineid"]
        if machine is None or vm_dict[machine]["status"] == "Stopped":
            print(
                textwrap.dedent(
                    f"""\
                ## - {volume['name']}
                # volume: {volume_id}
                """
                )
            )
            if machine is not None:
                print(
                    textwrap.dedent(
                        f"""
                    ## {volume['name']} is attached to {volume['vmname']}.
                    ## The default OS type will be {
                        vm_dict[machine]['_guestosid']}
                    """
                    )
                )
    print(
        textwrap.dedent(
            """\
        ### OS Type
        ## Uncomment one if the volume is not attached to a VM or if you want to
        ## declare a different OS type.
        """
        )
    )
    for os_typeid, os_type in os_types.items():
        print(
            textwrap.dedent(
                f"""
            ## - {os_type}
            # os-type: {os_typeid}
            """
            )
        )
