"""Managing virtual machines"""

import sys
import textwrap
import time
import typing
import webbrowser

import bs4
import cs.client
import requests
import yaml

import api.portal
import commandline
import connections
import tables
import projects

# See https://github.com/python/typeshed/issues/3500#issuecomment-560958608
if sys.version_info >= (3, 8):
    typing_extensions = typing  # pylint: disable=invalid-name
else:
    import typing_extensions


def complete_offering(kind, parsed_args):
    """return possible completions for a command-line argument containing an
    offer of kind."""
    project = parsed_args.project
    with connections.NoninteractiveConnections(
        parsed_args.credentials
    ) as connection:
        return connection.portal.projects[project].vms.create()[kind].values()


def make_complete_offering(kind):
    """return a completer for a given kind"""

    def complete(**kwargs):
        parsed_args = kwargs["parsed_args"]
        return complete_offering(kind, parsed_args)

    return complete


def complete_create(**kwargs):
    """completer for create command"""
    parsed_args = kwargs["parsed_args"]
    step = commandline.get_complete_argument_index(parsed_args)
    if step == 1:  # template
        return complete_offering("all_templates", parsed_args)
    if step == 2:  # computer offering
        return complete_offering("computer_offerings", parsed_args)
    return None


def get_vm_names(project, machine: api.portal.Vm):
    """return possible names for a given virtual machine"""
    hostname = machine.name
    prefix = project + "-"
    if hostname.startswith(prefix):
        return [hostname, hostname[len(prefix) :]]
    return [hostname]


def complete_name(**kwargs):
    """completer for virtual machine name"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials
    ) as connection:
        project = parsed_args.project
        return [
            vm_name
            for machine in connection.portal.projects[project].vms
            for vm_name in get_vm_names(project, machine)
        ]


Name = typing.NewType("Name", str)
Name.__doc__ = "virtual machine name"
commandline.set_completer(Name, complete_name)

TemplateName = typing.NewType("TemplateName", str)
TemplateName.__doc__ = textwrap.dedent(
    """\
    template name

    TEMPLATE can be an id or a regular expression that matches the corresponding
    template without ambiguity (the command will fail if there is no match or
    two matches or more).
    """
)
commandline.set_completer(TemplateName, make_complete_offering("all_templates"))

VmListFilter = typing.NewType("VmListFilter", str)
tables.init_list_filter_type(
    VmListFilter,
    list(api.crawler.VM_LIST_SPEC_MEMBER.keys())
    + [
        "guest_os",
        "_guest_os_id",
        "hypervisor",
        "domain",
        "_cpu_number",
        "_cpu_speed",
        "_memory",
    ],
)

Delete = typing.NewType("Delete", bool)
Delete.__doc__ = "delete virtual machine first if necessary"

VmCreateParameter = typing.NewType("VmCreateParameter", str)
VmCreateParameter.__doc__ = "[YAML-FILE | NAME TEMPLATE]"
commandline.set_completer(VmCreateParameter, complete_create)

DiskOffering = typing.NewType("DiskOffering", str)
DiskOffering.__doc__ = "disk offering"
commandline.set_completer(
    DiskOffering, make_complete_offering("disk_offerings")
)

ComputerOffering = typing.NewType("ComputerOffering", str)
ComputerOffering.__doc__ = "computer offering"
commandline.set_completer(
    ComputerOffering, make_complete_offering("computer_offerings")
)

CPUNumber = typing.NewType("CPUNumber", int)
CPUNumber.__doc__ = "CPU number"

Memory = typing.NewType("Memory", int)
Memory.__doc__ = "Memory (RAM) in GB"

RootDiskSize = typing.NewType("RootDiskSize", int)
RootDiskSize.__doc__ = "Root disk size in GB"

AdditionalDiskSize = typing.NewType("AdditionalDiskSize", int)
AdditionalDiskSize.__doc__ = "Additional disk size in GB"

Description = typing.NewType("Description", str)
Description.__doc__ = "virtual machine description"

CiAdmin = typing.NewType("CiAdmin", bool)
CiAdmin.__doc__ = "run request as CI admin"

Wait = typing.NewType("Wait", bool)
Wait.__doc__ = "wait for status change"

Force = typing.NewType("Force", bool)
Force.__doc__ = "force stop the VM"


def check_filter(filters, key) -> typing.Optional[str]:
    """Check if there is a filter of the form 'key=...'"""
    try:
        return next(
            vm_id
            for vm_id in (
                condition.get_simple_equality()
                for condition in filters.conditions
                if condition.key == key
            )
            if vm_id is not None
        )
    except StopIteration:
        return None


@commandline.RegisterCategory
class Vm(commandline.Category):
    """managing virtual machines"""

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    # pylint: disable=too-many-locals
    @commandline.RegisterAction(
        doc=f"""create a new virtual machine

        If no additional argument is provided, the command outputs a YAML
        template.
        This output can be redirected into a file to be filled and provided back
        to the command to perform the creation.

        The virtual machine specification can also be given directly in the
        command arguments.
        {api.spec.get_spec_text('Vm NAME', api.crawler.VM_NAME_SPEC)}
        TEMPLATE can be an id or a regular
        expressions that matches the corresponding offer name without ambiguity
        (the command will fail if there is no match or two matches or more).

        Defaults are 2GB memory, 1 CPU, 50GB root disk and no additional disk."""
    )
    def create(
        self,
        project: projects.Name,
        parameter: typing.List[VmCreateParameter],
        description: typing.Optional[Description] = None,
        memory: typing.Optional[Memory] = None,
        cpu_number: typing.Optional[CPUNumber] = None,
        root_disk_size: typing.Optional[RootDiskSize] = None,
        additional_disk_size: typing.Optional[AdditionalDiskSize] = None,
        wait: typing.Optional[Wait] = Wait(False),
    ) -> None:
        """create a new virtual machine"""
        if not parameter:
            with connections.Connections(self.config) as connection:
                print_yaml_template(
                    connection.portal.projects[project].vms.create()
                )
        else:
            if len(parameter) == 1:
                spec = load_spec_from_yaml(parameter[0])
                if description is not None:
                    raise commandline.Failure(
                        "--description cannot be used with YAML specification"
                    )
            else:
                try:
                    name, template = parameter
                except ValueError:
                    raise commandline.Failure(
                        """'vm create' expect 3 arguments:
                        PROJECT VM TEMPLATE"""
                    )
                memory_int = 2 if memory is None else int(memory)
                cpu_number_int = 1 if cpu_number is None else int(cpu_number)
                root_disk_size_int = (
                    50 if root_disk_size is None else int(root_disk_size)
                )
                description_str = "" if description is None else description
                spec = api.portal.Vm.Spec(
                    name=name,
                    description=description_str,
                    template=template,
                    memory=memory_int,
                    cpu_number=cpu_number_int,
                    root_disk_size=root_disk_size_int,
                    additional_disk_size=additional_disk_size,
                )
            api.spec.check_text_spec(
                spec.name, "virtual machine name", api.crawler.VM_NAME_SPEC
            )
            with connections.Connections(self.config) as connection:
                create = connection.portal.projects[project].vms.create()
                template_str = tables.find_offer(
                    create["all_templates"], spec.template, "template"
                )
                create.create(
                    api.portal.Vm.Spec(
                        name=spec.name,
                        description=spec.description,
                        template=template_str,
                        memory=spec.memory,
                        cpu_number=spec.cpu_number,
                        root_disk_size=spec.root_disk_size,
                        additional_disk_size=spec.additional_disk_size,
                    )
                )
                if not self.config.quiet:
                    commandline.output_message(
                        f"""
                        To restart Jenkins with the command-line:
                        ci jenkins restart {project}
                    """
                    )
                if wait:
                    wait_vm_state(connection, project, spec.name, "Running")

    # List source is a tagged union
    # https://mypy.readthedocs.io/en/latest/literal_types.html#tagged-unions

    class Portal(typing_extensions.TypedDict):
        """uses the portal"""

        source_type: typing_extensions.Literal["portal"]

    portal = Portal({"source_type": "portal"})

    class Cloudstack(typing_extensions.TypedDict):
        """uses the Cloudstack API: it is faster but requires Slave Admin
        rights, and destroyed virtual machines are hidden"""

        source_type: typing_extensions.Literal["cloudstack"]

    class Template(typing_extensions.TypedDict):
        """uses the Cloudstack API and shows only virtual machines,
        the template of which is TEMPLATE"""

        source_type: typing_extensions.Literal["template"]
        template: TemplateName

    class All(typing_extensions.TypedDict):
        """uses the Cloudstack API and shows destroyed virtual machines
        (requires to be CI administrator)"""

        source_type: typing_extensions.Literal["all"]

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        list_filter: typing.Iterable[VmListFilter],
        source: typing.Union[Portal, Cloudstack, Template, All] = portal,
        ci_admin: CiAdmin = CiAdmin(False),
    ) -> None:
        """list virtual machines

        If PROJECT is all-projects, all VMs are listed (CI admin only)."""
        filters = tables.Filters.parse(list_filter)
        all_projects = project == "all-projects"
        cloudstack = (
            source["source_type"] in ("cloudstack", "all", "template")
            or all_projects
        )
        show_all = source["source_type"] == "all"
        if (
            source["source_type"] in ("all", "portal")
            or all_projects
            or ci_admin
        ):
            connection = connections.Connections(self.config)
        else:
            connection = connections.Connections(self.config, subdomain=project)
        with connection:
            query_args = {}
            if show_all or ci_admin:
                query_args["domainid"] = connection.get_project_domainid(
                    project
                )
            if all_projects:
                query_args["listAll"] = True
            if source["source_type"] == "template":
                create = connection.portal.projects[project].vms.create()
                query_args["templateid"] = tables.find_offer(
                    create["all_templates"], source["template"], "template"
                )
            if cloudstack:
                vm_id = check_filter(filters, "id")
                if vm_id is not None:
                    query_args["id"] = vm_id
                hypervisor = check_filter(filters, "hypervisor")
                if hypervisor is not None:
                    query_args["hypervisor"] = hypervisor
                rows = get_list_from_cloudstack(connection, **query_args)
            else:
                rows = tables.listable_to_table(
                    connection.portal.projects[project].vms
                )
        filters.show_list(rows, self.config.compact)

    @commandline.RegisterAction
    def start(
        self,
        project: projects.Name,
        machine: Name,
        wait: typing.Optional[Wait] = Wait(False),
    ) -> None:
        """start virtual machine"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].vms[machine].start()
            if wait:
                wait_vm_state(connection, project, machine, "Running")

    @commandline.RegisterAction
    def restart(
        self,
        project: projects.Name,
        machine: Name,
        wait: typing.Optional[Wait] = Wait(False),
    ) -> None:
        """restart virtual machine"""
        with connections.Connections(self.config) as connection:
            vm_obj = connection.portal.projects[project].vms[machine]
            vm_obj.stop()
            wait_vm_state(connection, project, machine, "Stopped")
            vm_obj.start()
            if wait:
                wait_vm_state(connection, project, machine, "Running")

    @commandline.RegisterAction
    def stop(
        self,
        project: projects.Name,
        machine: Name,
        wait: typing.Optional[Wait] = Wait(False),
        force: typing.Optional[Force] = Force(False),
        ci_admin: CiAdmin = CiAdmin(False),
    ) -> None:
        """stop virtual machine"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            if force:
                args = connection.cloudstack_args()
                vm_list = get_list_from_cloudstack(connection, **args)
                vm_id, _selected_vm = find(
                    tables.dict_of_list(vm_list, "id"),
                    project,
                    machine,
                    required=True,
                )
                connection.cloudstack.stopVirtualMachine(id=vm_id, forced=True)
            else:
                connection.portal.projects[project].vms[machine].stop()
                if wait:
                    wait_vm_state(connection, project, machine, "Stopped")

    @commandline.RegisterAction
    def delete(
        self,
        project: projects.Name,
        machine: Name,
        wait: typing.Optional[Wait] = Wait(False),
    ) -> None:
        """delete virtual machine"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].vms[machine].delete()
            if wait:
                wait_vm_state(connection, project, machine, "Destroyed")

    @commandline.RegisterAction
    def recover(self, project: projects.Name, machine: Name) -> None:
        """recover virtual machine"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].vms[machine].recover()

    @commandline.RegisterAction
    def expunge(
        self,
        project: projects.Name,
        machine: Name,
        delete: Delete = Delete(False),
    ) -> None:
        """really delete virtual machine"""
        with connections.Connections(self.config) as connection:
            domainid = connection.get_project_domainid(project)
            vm_list = get_list_from_cloudstack(connection, domainid=domainid)
            vm_id, selected_vm = find(
                tables.dict_of_list(vm_list, "id"),
                project,
                machine,
                required=True,
            )
            if delete and selected_vm["status"] != "Destroyed":
                self.delete(project, machine)
                while True:
                    time.sleep(1)
                    vm_list = get_list_from_cloudstack(
                        connection, domainid=domainid, id=vm_id
                    )
                    if vm_list[0] == "Destroyed":
                        break
            try:
                connection.cloudstack.expungeVirtualMachine(id=vm_id)
            except cs.client.CloudStackApiException as error:
                if error.error["errorcode"] == 530 and not delete:
                    raise commandline.Failure(
                        textwrap.dedent(
                            f"""\
                        {error.error["errortext"]}.
                        Hint: use expunge --delete option.
                    """
                        )
                    )
                raise error

    @commandline.RegisterAction
    def console(
        self,
        project: projects.Name,
        machine: Name,
        ci_admin: CiAdmin = CiAdmin(False),
    ) -> None:
        """open default web browser to show virtual machine console"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            vm_list = get_list_from_cloudstack(connection, **args)
            vm_id, _selected_vm = find(
                tables.dict_of_list(vm_list, "id"),
                project,
                machine,
                required=True,
            )
            params = {"cmd": "access", "vm": vm_id}
            response = connection.cloudstack.session.send(
                requests.Request(
                    "GET", connection.cloudstack_console_url, params=params
                )
            )
            api.crawler.check_server_response(response)
            page = bs4.BeautifulSoup(response.content, features="lxml")
            frame = page.find("frame")
            if not isinstance(frame, bs4.element.Tag):
                raise Exception("Expected frame")
            url = api.crawler.get_single_attribute(frame, "src")
            webbrowser.open(url)

    @commandline.RegisterAction
    def scale(
        self,
        project: projects.Name,
        machine: Name,
        offer: typing.Optional[ComputerOffering] = None,
        cpu_number: typing.Optional[CPUNumber] = None,
        memory: typing.Optional[Memory] = None,
        ci_admin: CiAdmin = CiAdmin(False),
    ) -> None:
        """change virtual machine computer offering"""
        with connections.Connections(
            self.config, subdomain=project, ci_admin=ci_admin
        ) as connection:
            args = connection.cloudstack_args()
            vm_list = get_list_from_cloudstack(connection, **args)
            vm_id, _selected_vm = find(
                tables.dict_of_list(vm_list, "id"),
                project,
                machine,
                required=True,
            )
            create = connection.portal.projects[project].vms.create()
            if offer is None:
                offer_str = "Custom"
            else:
                offer_str = offer
            service_offering_id = tables.find_offer(
                create["computer_offerings"], offer_str, "computer offering"
            )
            details = {}
            if cpu_number is not None:
                details["details[0].cpuNumber"] = str(cpu_number)
            if memory is not None:
                # details[0].memory is in MB and Memory type is documented as GB
                details["details[0].memory"] = str(int(memory) * 1024)
            connection.cloudstack.scaleVirtualMachine(
                id=vm_id, serviceofferingid=service_offering_id, **details
            )


def load_spec_from_yaml(filename: str) -> api.portal.Vm.Spec:
    """load virtual machine specification from Yaml file"""
    with open(filename) as f:
        spec = yaml.load(f, Loader=yaml.FullLoader)
    return api.portal.Vm.Spec(
        name=spec["name"],
        description=spec.get("description", ""),
        template=spec["template"],
        cpu_number=spec.get("cpu-number", 1),
        memory=spec.get("memory", 2),
        root_disk_size=spec.get("root-disk-size", 50),
        additional_disk_size=spec.get("additional-disk-size", None),
    )


def wait_vm_state(connection, project, machine, status):
    """wait for a given VM state"""
    while connection.portal.projects[project].vms[machine].status != status:
        time.sleep(1)


def print_comment(text):
    """Rewrap and print text, prefixing all lines with '# '."""
    print(textwrap.fill(text, initial_indent="# ", subsequent_indent="# "))


def print_yaml_template(create: api.portal.Vms.Create) -> None:
    """print YAML template for virtual machine creation on standard output"""

    def write_offers(field, dictionary):
        return "\n".join(
            [
                f"""
            ## - {text}
            # {field}: {id}"""
                for id, text in dictionary.items()
            ]
        )

    print_comment(
        api.spec.get_spec_text("Virtual Machine name", api.crawler.VM_NAME_SPEC)
    )
    print(
        textwrap.dedent(
            f"""\
            name:
            description: # optional

            ### Templates
            ## Uncomment one and only one `template: ...` line.

            ## Featured templates
            {write_offers('template', create['featured_templates'])}

            ## Community templates
            {write_offers('template', create['community_templates'])}

            ## Project templates
            {write_offers('template', create['project_templates'])}

            ## Optionally specify a custom disk size
            # root-disk-size: 50 # in GB

            ### Computer offering
            # cpu-number: 4 # number of cores, from 1 to 16 (default: 1)
            # memory: 2 # RAM, in GB, from 1GB to 24GB (default: 2GB)

            ### Disk offering (optional)
            # additionnal-disk-size: 50 # in GB, default: no additional disk
            """
        )
    )


def dict_of_os_types(connection):
    """enumerate OS types"""
    os_types = {}
    response = connection.cloudstack.listOsTypes(fetch_list=True)
    for ostype in response:
        os_types[ostype["id"]] = ostype["description"]
    return os_types


def get_list_from_cloudstack(connection, **args):
    """enumerate virtual machines using Cloudstack API"""
    vms = connection.cloudstack.listVirtualMachines(fetch_list=True, **args)
    result = []
    os_types = dict_of_os_types(connection)
    for machine in vms:
        guestosid = machine["guestosid"]
        result.append(
            {
                "id": machine["id"],
                "status": machine["state"],
                "display_name": machine["displayname"],
                "hostname": machine["name"],
                "ip": machine["nic"][0].get("ipaddress", ""),
                "os": machine["templatedisplaytext"],
                "cpu": machine["cpuspeed"],
                "memory": machine["memory"],
                "created": machine["created"],
                "guest_os": os_types[guestosid],
                "_guest_os_id": guestosid,
                "hypervisor": machine["hypervisor"],
                "domain": machine["domain"],
                "_cpu_number": machine["cpunumber"],
                "_cpu_speed": machine["cpuspeed"],
                "_memory": machine["memory"],
            }
        )
    return result


def find(vms, project, vm_name, required=False):
    """find a virtual machine"""
    if vm_name in vms:
        return vm_name
    for vm_id, machine in vms.items():
        if machine["hostname"] == vm_name:
            return vm_id, machine
    full_vm_name = f"{project}-{vm_name}"
    for vm_id, machine in vms.items():
        if machine["hostname"] == full_vm_name:
            return vm_id, machine
    for vm_id, machine in vms.items():
        if machine["display_name"] == vm_name:
            return vm_id, machine
    for vm_id, machine in vms.items():
        if machine["display_name"] == full_vm_name:
            return vm_id, machine
    if required:
        raise commandline.Failure(
            f"Project {project} does not have vm {vm_name}."
        )
    return None
