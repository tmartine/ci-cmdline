#!/bin/bash
set -e
admin_credentials="$HOME/secrets/thierry_martinez_ci_credentials.yml"
export CI_CREDENTIALS="$admin_credentials"
ci project create test-ci-cmdline "test-ci-cmdline" "Test project for CI cmdline"
